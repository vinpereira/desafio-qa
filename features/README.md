# Concrete Solutions - QA Test - Part 1

- Write two features for Whatsapp using Gherkin or Gauge

# Considerations
  - I wrote the features using the declarative style
  	- Because I consider it the most faithful representation of a request made by the user

  - In the imperative style, used to learn the Gherkin, the focus is to explain how a functionality should be done

  - In the declarative style, the most indicated by authors and the Gherkin documentation, the focus is to explain what a functionality must satisfy
  	- In my opinion, this is the best way to describe a feature that a user wants

