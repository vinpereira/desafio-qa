Feature: Manage a group
	As a user
	I want to have a conversation with some friends at the same time
	So I can keep in touch with everyone in a more practical way

	Background:
		Given I open the Whatsapp

	Scenario: Create a new group
		In this scenario a new group is created with some friends

		Given I want to create a new group
	 	 When I create a new grup
	 	  And I add some friends to this group
	 	 Then I should see a confirmation that the group was created
	 	  And I should see the friends that I added in this group


	Scenario: Leave a group
		In this scenario the user select a group and leave it

		Given I select a group
		 When I leave it
		 Then I should not be able to write on it
		  And I should not receive messages from it
	 	 