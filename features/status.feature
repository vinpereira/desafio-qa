Feature: Manage my status
	As a user
	I want to control my status message
	In order to express my mood

	Scenario: Update my status
		In this scenario the user wants to update his status

		Given I open the Whatsapp
	 	 When I access my status
	 	  And I changed it
	 	 Then I should see my new status message
