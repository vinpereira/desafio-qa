Feature: Have a conversation
	As a user
	I want to have a conversation with a friend
	So I can keep in touch in a more practical way

	Background:
		Given I open the Whatsapp

	Scenario: Send a conversation by text
		In this scenario only a text conversation is considered
		and the confirmation should be a successfully

		
	 	Given I select a friend
	 	 When I send him a text message
	 	 Then I should see a confirmation that the message was sent


	Scenario: Error - Send a conversation by text
		In this scenario only a text conversation is considered
		and an error occurs

	 	Given I select a friend
	 	 When I send him a text message
	 	  And the message could not be delivery
	 	 Then I should see message saying "Failed to delivery - Try again"
