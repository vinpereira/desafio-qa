require 'test/unit/testsuite'
require 'test/unit/ui/console/testrunner'
require './test_price.rb'

# Class with the Test Suite to run the Unit Test (test_price.rb)
class CheckoutTestSuite

  def self.suite
    suite = Test::Unit::TestSuite.new
    suite << TestPrice.suite
    return suite
  end

end # end-class

Test::Unit::UI::Console::TestRunner.run(CheckoutTestSuite)