# Concrete Solutions - QA Test - Part 2

- Write a code to solve the CodeKata 09 challenge

# Programming Language and libraries for the CodeKata 09

  - Ruby 2.4.1 @ Ubuntu 16.10 64-bits

Main libraries:
  - test-unit 3.2.3
  
### Running the solution for CodeKata 09:
Go to the codekata09 folder and execute the following code:

```sh
$ ruby test_suite.rb
```
