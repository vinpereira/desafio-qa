# Class to manage the price policy of a product
class PricePolicy

  def initialize(starts_at, *discounts)
    @starts_at = starts_at
    @discounts = discounts
  end

  def price_for(quantity)
    quantity * @starts_at - discount_for(quantity)
  end

  def discount_for(quantity)
    @discounts.inject(0) do |mem, discount|
      mem + discount.calculate_for(quantity)
    end
  end

end # end-class